# PHP Package for WikiSuite

This package would automatically force the installation of all required PHP packages and will provide some default configurations for PHP


If you need to support a new or different versions of PHP:
1. Edit `generate_debian_config.sh` and add the version to `PHP_VERSIONS`
2. Run `generate_debian_config.sh` that will generate all files needed
3. Commit the changes (files added and/or removed)
