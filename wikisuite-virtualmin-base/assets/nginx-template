# -- Prevent Browsing of Certain File Extensions -- #
location ~ "\.(bak|inc|lib|sh|tpl|sql|shtml|asp|xml\.dist)$" {
    deny all;
}

# -- Prevent Browsing of Certain File Names -- #
location ~ "(changelog\.txt|_htaccess|README|INSTALL|web_config|copyright\.txt|composer\..*)$" {
    deny all;
}

# -- Home Page Feature -- #
index index.php index.html;
#index tiki-index.php index.php index.html;


# -- Ensure Caching Proxy Sends Content to Correct Client -- #
gzip_vary on;
gzip on;
gzip_types text/css text/x-component application/x-javascript application/javascript text/javascript text/x-js text/html text/richtext image/svg+xml text/plain text/xsd text/xsl text/xml image/x-icon application/json;

# -- Client Cache Method -- #
# FileETag none
etag off;

# -- httpoxy mitigation -- #
# RequestHeader unset Proxy early
fastcgi_param HTTP_PROXY "";

# -- Client Cache Expiration -- #
#Header unset Cache-Control
# Not changing this one in Nginx

# -- CORS-enabled Images (@crossorigin) --
# <FilesMatch "(?i)\.(gif|png|jpe?g|svgz?|ico)$">
#  SetEnvIf Origin ":" IS_CORS
# Header set Access-Control-Allow-Origin "*" env=IS_CORS
location ~* "\.(gif|png|jpe?g|svgz?|ico)$" {
    add_header Access-Control-Allow-Origin "*";
    expires 30d; # part of Expires Headers
}

# -- Webfont Access -- #
# <FilesMatch "(?i)\.(ttf|ttc|otf|eot|woff2?)$">
# Header set Access-Control-Allow-Origin "*"
location ~* "\.(ttf|ttc|otf|eot|woff2?)$" {
    add_header Access-Control-Allow-Origin "*";
    expires 30d; # part of Expires Headers
}

# -- Enable video/audio seek -- #
# Header set Accept-Ranges bytes
add_header Accept-Ranges "bytes";

# -- Expires Headers -- #
# <FilesMatch "(?i)\.(gif|png|jpe?g|svgz?|ico)$">
# <FilesMatch "(?i)\.(js|css)$">
# <FilesMatch "(?i)\.(woff|woff2)$">
# ExpiresDefault "access plus 1 month"

# not using map because needs to sit outside of server directive
#map $request_uri $expires {
#    default         off;
#    ~*\.(gif|png|jpe?g|svgz?|ico)$ 30d;
#    ~*\.(js|css)$ 30d;
#    ~*\.(woff|woff2)$ 30d;
#}
#expires $expires;
location ~* "\.(js|css)$" {
    expires 30d;
}

# Redirect drawio requests to the correct path
# RewriteRule ^img/lib/(.*)$ vendor/tikiwiki/diagram/img/lib/$1 [NC,L]
rewrite ^/img/lib/(.*)$ /vendor/tikiwiki/diagram/img/lib/$1 last;

# -- Apache Authorization Header -- #
# RewriteCond %{HTTP:Authorization} ^(.*)
# RewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]
fastcgi_param HTTP_AUTHORIZATION $http_authorization;

# -- Set these headers for Last-Modified and Etag in wiki pages -- #
# RewriteCond %{HTTP:If-Modified-Since} ^(.*)
# RewriteRule ^(.*) - [E=HTTP_IF_MODIFIED_SINCE:%1]
# RewriteCond %{HTTP:If-None-Match} ^(.*)
# RewriteRule ^(.*) - [E=HTTP_IF_NONE_MATCH:%1]
fastcgi_param HTTP_IF_NONE_MATCH $http_if_none_match;
fastcgi_param HTTP_IF_MODIFIED_SINCE $http_if_modified_since;

# -- Prevent HTTP TRACE method -- #
# RewriteCond %{REQUEST_METHOD} ^TRACE
# RewriteRule .* - [F]
if ($request_method ~ ^(PATCH|TRACE)$) {
    return 405;
}

# -- Disallow access to hidden files (apart from well-known ones) -- #
# RewriteRule "(^|/)\.(?!well-known/)" - [F,L]
location ~ "(^|/)\.(?!well-known/)" {
    deny all;
}

# Redirect robots.txt to dynamic tiki-robots.php file.
# RewriteRule ^robots\.txt$ route.php [L]
rewrite ^robots\.txt$ route.php last;

# -- If the URL Points to a File Then do Nothing -- #
location / {
    try_files $uri $uri/ "/route.php?q=$uri&$args";
}

# -- CalDAV/CardDAV service auto-discovery -- #
# RewriteRule ^.well-known/caldav$    tiki-caldav.php [R=301,L]
# RewriteRule ^.well-known/carddav$   tiki-carddav.php [R=301,L]
location "/.well-known/caldav" {
    return 301 tiki-caldav.php;
}
location "/.well-known/carddav" {
    return 301 tiki-carddav.php;
}


# -- Tiki URL Rewriting -- #
# RewriteRule .*                     route.php [L]
# handled by tryfiles

#
# temp Folder Rules
# allow png, html, js, css, pdf, deny the rest
location ~ "^/temp/.*\.(png|html|js|css|pdf)$" {
   allow all;
}
location ~ "^/temp(|/.*)$" {
   deny all;
}
